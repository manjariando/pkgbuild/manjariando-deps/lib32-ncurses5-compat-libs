# Maintainer: Kevin Brodsky <corax26 at gmail dot com>

_pkgname=ncurses
pkgname=lib32-${_pkgname}5-compat-libs
pkgver=6.3
pkgrel=2.2
pkgdesc="System V Release 4.0 curses emulation library (32-bit), ABI 5"
arch=('x86_64')
url="https://www.gnu.org/software/ncurses/"
license=('MIT')
makedepends=('lib32-gcc-libs')
depends=('lib32-glibc' "lib32-${_pkgname}")
source=("https://invisible-mirror.net/archives/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.asc}
        "ncurses-6.3-libs.patch" "ncurses-6.3-pkgconfig.patch")
sha512sums=('5373f228cba6b7869210384a607a2d7faecfcbfef6dbfcd7c513f4e84fbd8bcad53ac7db2e7e84b95582248c1039dcfc7c4db205a618f7da22a166db482f0105'
            'SKIP'
            'adb02b838c40f1e58a1b31c26d5cd0f2a1c43f3b6d68e839981764c0f6c905a9eb51dd36ff018628fdeb20747cc7467727d57135408ab4848259384077a52b28'
            '2d2c0ec3c880e638ab4aa3dbff5e28e4cd233153e24816bd87e077f848aa3edd5114cd0f2a7f6e8869dd1861a2746e512886c18264ff1676927dcc320c5ef958')
validpgpkeys=('19882D92DDA4C400C22C0D56CC2AF4472167BE03')  # Thomas Dickey <dickey@invisible-island.net>

prepare() {
  cd ${_pkgname}-$pkgver
  # do not link against test libraries
  patch -Np1 -i ../"${_pkgname}-6.3-libs.patch"
  # do not leak build-time LDFLAGS into the pkgconfig files:
  # https://bugs.archlinux.org/task/68523
  patch -Np1 -i ../"${_pkgname}-6.3-pkgconfig.patch"
  # NOTE: can't run autoreconf because the autotools setup is custom and ancient
}

build() {
  cd ${_pkgname}-${pkgver}

  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_LIBDIR='/usr/lib32/pkgconfig'

  ./configure \
     --prefix=/usr \
     --disable-db-install \
    --enable-widec \
    --enable-pc-files \
    --libdir=/usr/lib32 \
    --mandir=/usr/share/man \
    --with-cxx-binding \
    --with-cxx-shared \
    --with-pkg-config-libdir=/usr/lib32/pkgconfig \
    --with-shared \
    --with-versioned-syms \
    --without-ada \
    --without-debug \
    --without-manpages \
    --without-progs \
    --without-tack \
    --without-tests \
     --with-abi-version=5
   make
}

package() {
  cd ${_pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install.libs

  install -vDm 644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname/"

  # fool packages looking to link to non-wide-character ncurses libraries
  for lib in ncurses form panel menu; do
    ln -s lib${lib}w.so.5 "$pkgdir"/usr/lib32/lib${lib}.so.5
  done

  for lib in tic tinfo; do
    ln -s libncursesw.so.5 "${pkgdir}/usr/lib32/lib${lib}.so.5"
   done

  # Remove .so symlinks and static libraries (conflicting with lib32-ncurses)
  rm -f "${pkgdir}"/usr/{lib32/*.so,lib32/*.a}

  # remove all files conflicting with ncurses
  rm -frv "${pkgdir}/usr/"{bin,include,lib32/pkgconfig}
}
